# default INKBRANCH to "master"
ifneq ($(MAKECMDGOALS), help)
ifndef INKBRANCH
$(warning 'INKBRANCH' is not set, assuming 'master')
override INKBRANCH := master
endif
endif

PO_FILES := $(wildcard *.po)
LANGS := $(PO_FILES:.po=)

authors_include_tag := \$${INKSCAPE_AUTHORS}
authors_include_tag_old := [% INCLUDE \"AUTHORS\" %]
authors_list := $$(less AUTHORS | awk -vORS=", " '{ print $$$$0 }' | sed 's/, $$$$/\n/')


.PHONY: all
all: pod get_AUTHORS


# rule to download the latest AUTHORS file from the inkscape git repository
AUTHORS: get_AUTHORS

.PHONY: get_AUTHORS
get_AUTHORS:
ifdef INKBRANCH
	wget -qN https://gitlab.com/inkscape/inkscape/-/raw/$(INKBRANCH)/AUTHORS -O AUTHORS.new
else
	@echo WARNING: 'INKBRANCH' is not set, downloading AUTHORS from 'master'
	wget -qN https://gitlab.com/inkscape/inkscape/-/raw/master/AUTHORS -O AUTHORS.new
endif
	@diff AUTHORS AUTHORS.new > /dev/null && rm AUTHORS.new || mv AUTHORS.new AUTHORS


# rules for creating .pod.in files (including .pod files for old autotools builds) and .html files
pod: $(addprefix inkscape., $(addsuffix .pod.in, $(LANGS))) $(addprefix inkview., $(addsuffix .pod.in, $(LANGS))) AUTHORS
	@# generate the HTML version of the inkscape template POD file (strip empty <link> tag that causes validation errors)
	@pod2html --quiet --infile inkscape.pod.in --outfile inkscape-man.html --title "MAN PAGE"
	@sed -i "s/$(authors_include_tag)/$(authors_list)/g" inkscape-man.html
	@sed -i '/^<link rev="made" href="mailto:/d' inkscape-man.html

	@# generate the HTML version of the inkview template POD file (strip empty <link> tag that causes validation errors)
	@pod2html --quiet --infile inkview.pod.in --outfile inkview-man.html --title "MAN PAGE"
	@sed -i '/^<link rev="made" href="mailto:/d' inkview-man.html

	@rm -r *.tmp

inkscape.%.pod.in: %.po AUTHORS
	po4a-translate -f pod -m inkscape.pod.in -p $*.po -l inkscape.$*.pod.in || exit 0

	@if [ -f inkscape.$*.pod.in ]; \
	then \
		# generate the HTML versions of the translated inkscape POD files (strip empty <link> tag that causes validation errors) \
		pod2html --quiet --infile inkscape.$*.pod.in --outfile inkscape-man.$*.html --title "MAN PAGE"; \
		sed -i "s/$(authors_include_tag)/$(authors_list)/g" inkscape-man.$*.html; \
		sed -i '/^<link rev="made" href="mailto:/d' inkscape-man.$*.html; \
	fi

inkview.%.pod.in: %.po
	po4a-translate -f pod -m inkview.pod.in -p $*.po -l inkview.$*.pod.in || exit 0

	@if [ -f inkview.$*.pod.in ]; \
	then \
		# generate the HTML versions of the translated inkview POD files (strip empty <link> tag that causes validation errors) \
		pod2html --quiet --infile inkview.$*.pod.in --outfile inkview-man.$*.html --title "MAN PAGE"; \
		sed -i '/^<link rev="made" href="mailto:/d' inkview-man.$*.html; \
	fi



# rules for (re-)creating .pot and .po files
.PHONY: pot
pot: man.pot

man.pot: inkscape.pod.in inkview.pod.in
	@rm -f man.pot
	po4a-updatepo -f pod -m inkscape.pod.in -m inkview.pod.in -p man.pot
	@rm -f man.pot~

.PHONY: po
po: *.po

%.po: man.pot
	msgmerge $*.po man.pot > $*.new.po
	@mv $*.new.po $*.po



# check input files
.PHONY: check-input
check-input:
	echo "TODO: target 'check-input' not coded yet"

# check output files
.PHONY: check-output
check-output: check-output-html

.PHONY: check-output-html
check-output-html: pod
	tidy -e -q *.html



.PHONY: copy
copy:
	mkdir -p ../export-website/man
	for file in inkscape-man*.html; do cp "$$file" "../export-website/man/inkscape-man-$(INKBRANCH)$${file#inkscape-man}"; done
	for file in inkview-man*.html; do cp "$$file" "../export-website/man/inkview-man-$(INKBRANCH)$${file#inkview-man}"; done
	mkdir -p ../export-inkscape/man
	cp *.pod.in ../export-inkscape/man

.PHONY: clean
clean:
	rm -f AUTHORS
	rm -f *.*.pod.in
	rm -f *.html

.PHONY: help
help:
	@echo "Targets:"
	@echo "   all          - Create man pages (.pod and .html files) from inkscape.pod.in"
	@echo "   check-input  - Validate (non-compiled) input files"
	@echo "   check-output - Validate (compiled) output files"
	@echo "   pot          - Update man.pot"
	@echo "   po           - Update man.pot and all po files"
	@echo "   copy         - Copy generated HTML man pages to the export directory"
	@echo "   clean        - Remove all generated files"
